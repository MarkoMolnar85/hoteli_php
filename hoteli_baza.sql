--
-- Database: `hoteli_baza`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `korisnik_id` int(11) NOT NULL,
  `korisnicko_ime` varchar(25) COLLATE utf8_croatian_ci NOT NULL,
  `ime` varchar(25) COLLATE utf8_croatian_ci NOT NULL,
  `sifra` varchar(30) COLLATE utf8_croatian_ci NOT NULL,
  `e_mail` varchar(30) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`korisnik_id`, `korisnicko_ime`, `ime`, `sifra`, `e_mail`) VALUES
(1, 'marko', 'marko molnar', '123', ''),
(2, 'marko12', 'marko', 'marko', 'marko@'),
(3, 'nikola', 'nikola molnar', 'nikola', 'nikola@da'),
(5, 'petar', 'petar', 'petar', 'petar'),
(11, 'Stefan', 'Stefan Jokic', '123', 'mail@gmail.com'),
(12, 'Milos', 'Bajovic', '111', 'milos@gmail.com'),
(13, 'Milica81', 'Milica Uzunovic', '555', 'milica@gmail.com'),
(14, 'Zdravko', 'Zdravko Colic', '951', 'cola@hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `poruka_db`
--

CREATE TABLE `poruka_db` (
  `poruka_id` int(11) NOT NULL,
  `pruka_text` varchar(250) COLLATE utf8_croatian_ci NOT NULL,
  `korisnicko_ime` varchar(25) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `poruka_db`
--

INSERT INTO `poruka_db` (`poruka_id`, `pruka_text`, `korisnicko_ime`) VALUES
(1, 'Ovo je text poruka', 'Stefan'),
(2, 'Mnogo dobar sajt bre', 'marko'),
(3, 'Nisam zaovoljan imaju samo dva hotela. ne svidja mi se boja pozadine.', 'Milos'),
(4, 'Nije lose moze i bolje', 'Milica81'),
(5, 'Zadovoljan sam izborom', 'Zdravko');

-- --------------------------------------------------------

--
-- Table structure for table `rezervacija_db`
--

CREATE TABLE `rezervacija_db` (
  `rezervacija_id` int(11) NOT NULL,
  `pocetni_datum` varchar(10) COLLATE utf8_croatian_ci NOT NULL,
  `krajnji_datum` varchar(10) COLLATE utf8_croatian_ci NOT NULL,
  `korisnicko_ime` varchar(25) COLLATE utf8_croatian_ci NOT NULL,
  `broj_sobe` varchar(3) COLLATE utf8_croatian_ci NOT NULL,
  `hotel_ime` varchar(20) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `rezervacija_db`
--

INSERT INTO `rezervacija_db` (`rezervacija_id`, `pocetni_datum`, `krajnji_datum`, `korisnicko_ime`, `broj_sobe`, `hotel_ime`) VALUES
(11, '2016-06-13', '2016-06-16', 'Stefan', '112', 'more'),
(12, '2016-06-13', '2016-06-20', 'Stefan', '111', 'more'),
(13, '2016-06-22', '2016-06-30', 'Stefan', '212', 'Planina'),
(14, '2016-07-22', '2016-07-26', 'marko', '213', 'Planina'),
(15, '2016-08-09', '2016-08-16', 'Milos', '112', 'more'),
(16, '2016-10-10', '2016-10-16', 'Milica81', '212', 'Planina'),
(17, '2016-11-11', '2016-11-18', 'Zdravko', '214', 'Planina');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`korisnik_id`),
  ADD KEY `korisnicko_ime` (`korisnicko_ime`);

--
-- Indexes for table `poruka_db`
--
ALTER TABLE `poruka_db`
  ADD PRIMARY KEY (`poruka_id`),
  ADD KEY `korisnicko_ime` (`korisnicko_ime`);

--
-- Indexes for table `rezervacija_db`
--
ALTER TABLE `rezervacija_db`
  ADD PRIMARY KEY (`rezervacija_id`),
  ADD KEY `korisnicko_ime` (`korisnicko_ime`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `korisnik_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `poruka_db`
--
ALTER TABLE `poruka_db`
  MODIFY `poruka_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `rezervacija_db`
--
ALTER TABLE `rezervacija_db`
  MODIFY `rezervacija_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
